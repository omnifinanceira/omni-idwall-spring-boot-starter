package br.com.omni.idwall.starter.api.util;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import org.springframework.web.client.HttpClientErrorException;

import br.com.omni.commons.exception.UnauthorizedException;
import br.com.omni.commons.http.domain.response.DefaultResponse;
import br.com.omni.commons.json.JsonUtils;
import br.com.omni.idwall.starter.exception.NegocioException;
import br.com.omni.idwall.starter.exception.SelfieIdwallNotFoundException;

public class IdwallWSUtil {

    private IdwallWSUtil(){}

    public static RuntimeException excecaoDeNegocio(HttpClientErrorException e) {
        if (e.getStatusCode().equals(NOT_FOUND))
            return new SelfieIdwallNotFoundException();

        if (e.getStatusCode().equals(UNAUTHORIZED))
            return new UnauthorizedException();

        if(e.getStatusCode().equals(BAD_REQUEST)){
            try {
                DefaultResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), DefaultResponse.class);
                if(response.getMessages()!=null && !response.getMessages().isEmpty())
                    return  new NegocioException(response.getMessages().get(0));
                else
                    return  new NegocioException(response.getStatus());
            }catch (Exception ex){
                return new RuntimeException(ex) ;
            }

        }
        return e;
    }
}
