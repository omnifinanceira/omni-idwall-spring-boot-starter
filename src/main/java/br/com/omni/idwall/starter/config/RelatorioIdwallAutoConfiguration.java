package br.com.omni.idwall.starter.config;

import br.com.omni.idwall.starter.api.RelatorioIdwall;
import br.com.omni.idwall.starter.config.amqp.ProducerBase;
import br.com.omni.idwall.starter.config.amqp.RelatorioIdwallQueuesConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({RelatorioIdwallQueuesConfiguration.class})
public class RelatorioIdwallAutoConfiguration {

    @Value("${spring.application.name}")
    private final String replyToDefault;

    @Autowired
    public RelatorioIdwallAutoConfiguration(@Value("${spring.application.name}") String replyToDefault) {
        this.replyToDefault = replyToDefault;
    }

    @Bean(name = "ProducerBaseRelatorioIdwall")
    @ConditionalOnMissingBean
    public ProducerBase producerBase(ObjectMapper objectMapper){
        return new ProducerBase(objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public RelatorioIdwall relatorioIdwall(@Qualifier("rabbitTemplateCommon") RabbitTemplate template, ProducerBase producerBase){
        return new RelatorioIdwall(template, producerBase, replyToDefault);
    }

}
