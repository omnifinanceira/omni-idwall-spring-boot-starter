package br.com.omni.idwall.starter.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import br.com.omni.autenticacao.starter.http.client.AutenticacaoWS;
import br.com.omni.idwall.starter.http.client.IdWallReportWS;
import br.com.omni.idwall.starter.http.client.impl.IdWallReportWSImpl;

@Configuration
@EnableConfigurationProperties(IdwallApiProperties.class)
public class IdwallReportApiConfiguration {

	private final IdwallApiProperties properties;
	private final RestTemplate restTemplate;
    private final AutenticacaoWS autenticacaoWS;

    public IdwallReportApiConfiguration(IdwallApiProperties properties, RestTemplate restTemplate,
			AutenticacaoWS autenticacaoWS) {
		this.properties = properties;
		this.restTemplate = restTemplate;
		this.autenticacaoWS = autenticacaoWS;
	}



	@Bean
	public IdWallReportWS idWallReportWS() {
		return new IdWallReportWSImpl(properties, restTemplate, autenticacaoWS);
	}

}
