package br.com.omni.idwall.starter.exception;

import br.com.omni.commons.exception.NotFoundException;

public class SelfieIdwallNotFoundException extends NotFoundException {
    public SelfieIdwallNotFoundException(String msg){ super(msg); }
    public SelfieIdwallNotFoundException(){ super(); }

}
