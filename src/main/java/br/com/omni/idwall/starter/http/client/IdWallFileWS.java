package br.com.omni.idwall.starter.http.client;

import org.springframework.core.io.Resource;

public interface IdWallFileWS {

	Resource findImage(String link);
	String findImageBase64Encoded(String link);
}
