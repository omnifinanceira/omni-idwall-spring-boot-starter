package br.com.omni.idwall.starter.api;

import br.com.omni.idwall.starter.config.amqp.EventMessage;
import br.com.omni.idwall.starter.config.amqp.EventMessageBuilder;
import br.com.omni.idwall.starter.config.amqp.ProducerBase;
import br.com.omni.idwall.starter.config.amqp.Queues;
import br.com.omni.idwall.starter.domain.MensagemRelatorioIdwall;
import br.com.omni.idwall.starter.domain.enums.TipoRelatorioIdwall;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.util.Arrays;

import static java.lang.String.format;

@RequiredArgsConstructor
@Component
public class RelatorioIdwall {

    public static final String BEAN_NAME = "relatorioIdwall";
    private final RabbitTemplate rabbitTemplate;
    private final ProducerBase producerBase;
    private final String replyToRoutingDefault;

    public void disparar(String tokenIdwall, TipoRelatorioIdwall tipoRelatorio) {
        MensagemRelatorioIdwall mensagem = buildMsgRelatorioIdwall(tokenIdwall, tipoRelatorio);

        String trackingId = format("IDWALL-REPORT-%s", tokenIdwall);

        EventMessage<MensagemRelatorioIdwall> event = new EventMessageBuilder()
            .trackingId(trackingId)
            .content(Arrays.asList(mensagem))
            .creationDate()
            .build();

        String idwallReportMessage = producerBase.stringifyRabbitMessage(event);

        MessagePostProcessor messagePostProcessor = new RelatorioIdwallMessagePostProcessor(replyToRoutingDefault);

        rabbitTemplate.convertAndSend(Queues.EXCHANGE_REQUEST, Queues.ROUTING_KEY_REQUEST, idwallReportMessage, messagePostProcessor);
    }

    public void dispararComImagens(String cpf, Long nsuImgSelfie, Long nsuImgDocFrente, Long nsuImgDocVerso, Long nsuImgDocCompleta) {
        MensagemRelatorioIdwall mensagem = buildMsgRelatorioIdwallImagens(cpf, nsuImgSelfie, nsuImgDocFrente, nsuImgDocVerso, nsuImgDocCompleta);

        String trackingId = format("IDWALL-REPORT-%s", cpf);

        EventMessage<MensagemRelatorioIdwall> event = new EventMessageBuilder()
            .trackingId(trackingId)
            .content(Arrays.asList(mensagem))
            .creationDate()
            .build();

        String idwallReportMessage = producerBase.stringifyRabbitMessage(event);

        MessagePostProcessor messagePostProcessor = new RelatorioIdwallMessagePostProcessor(replyToRoutingDefault);

        rabbitTemplate.convertAndSend(Queues.EXCHANGE_REQUEST, Queues.ROUTING_KEY_REQUEST, idwallReportMessage, messagePostProcessor);

    }

    private MensagemRelatorioIdwall buildMsgRelatorioIdwall(String tokenIdwall, TipoRelatorioIdwall tipoRelatorio) {
        MensagemRelatorioIdwall mensagemRelatorioIdwall = new MensagemRelatorioIdwall();
        mensagemRelatorioIdwall.setTipoRelatorioIdwall(tipoRelatorio);
        mensagemRelatorioIdwall.setTokenIdwall(tokenIdwall);
        return mensagemRelatorioIdwall;
    }

    private MensagemRelatorioIdwall buildMsgRelatorioIdwallImagens(String cpf, Long nsuImgSelfie, Long nsuImgDocFrente, Long nsuImgDocVerso, Long nsuImgDocCompleta) {
        MensagemRelatorioIdwall mensagemRelatorioIdwall = new MensagemRelatorioIdwall();
        mensagemRelatorioIdwall.setTipoRelatorioIdwall(TipoRelatorioIdwall.FACEMATCH_OCR_IMAGENS);
        mensagemRelatorioIdwall.setTokenIdwall(cpf);
        mensagemRelatorioIdwall.setCpf(cpf);
        mensagemRelatorioIdwall.setNsuImgSelfie(nsuImgSelfie);
        mensagemRelatorioIdwall.setNsuImgDocFrente(nsuImgDocFrente);
        mensagemRelatorioIdwall.setNsuImgDocVerso(nsuImgDocVerso);
        mensagemRelatorioIdwall.setNsuImgDocCompleta(nsuImgDocCompleta);
        return mensagemRelatorioIdwall;
    }
}
