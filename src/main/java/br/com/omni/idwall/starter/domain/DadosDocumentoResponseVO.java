package br.com.omni.idwall.starter.domain;

import br.com.omni.idwall.starter.domain.enums.StatusDocumento;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class DadosDocumentoResponseVO implements Serializable {

    private Long id;
    private String nome;
    private String rg;
    private String orgaoEmissorRg;
    private String estadoEmissaoRg;
    private String cpf;
    private LocalDate dataNascimento;
    private String nomePai;
    private String nomeMae;
    private String cnh;
    private LocalDate dataEmissaoCnh;
    private String orgaoEmissorCnh;
    private String localEmissaoCnh;
    private String estadoEmissaoCnh;
    private LocalDate dataExpedicaoRg;
    private String naturalidade;
    private StatusDocumento statusDocumento;

}
