package br.com.omni.idwall.starter.http.client;

import br.com.omni.idwall.starter.domain.FindSelfieURLResponse;

public interface IdWallReportWS {
	FindSelfieURLResponse findSelfieUrl(String token);
}
