package br.com.omni.idwall.starter.domain.enums;

public enum TipoRelatorioIdwall {
    FACEMATCH_OCR_RG, FACEMATCH_OCR_CNH, FACEMATCH_OCR_IMAGENS
}
