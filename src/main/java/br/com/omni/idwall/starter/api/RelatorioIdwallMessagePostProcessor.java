package br.com.omni.idwall.starter.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Address;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

import static br.com.omni.idwall.starter.config.amqp.Queues.EXCHANGE_RESPONSE;

@RequiredArgsConstructor
class RelatorioIdwallMessagePostProcessor implements MessagePostProcessor {

    @Getter
    private final String replyRouting;

    @Override
    public Message postProcessMessage(Message message) {
        message.getMessageProperties().setContentType("application/json");
        message.getMessageProperties().setReplyToAddress(new Address(EXCHANGE_RESPONSE, replyRouting));
        return message;
    }
}
