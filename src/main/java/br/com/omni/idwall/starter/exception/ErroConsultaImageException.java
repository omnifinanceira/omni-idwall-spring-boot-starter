package br.com.omni.idwall.starter.exception;

public class ErroConsultaImageException extends RuntimeException {

	public ErroConsultaImageException() {}
	
	public ErroConsultaImageException(String message) {
		super(message);
	}
	public ErroConsultaImageException(Exception e) {
		super(e);
	}
	
	
	
}
