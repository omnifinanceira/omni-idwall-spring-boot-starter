package br.com.omni.idwall.starter.http.client.impl;

import static br.com.omni.commons.json.JsonUtils.toJson;
import static java.util.Arrays.asList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.omni.autenticacao.starter.exception.TokenException;
import br.com.omni.autenticacao.starter.http.client.AutenticacaoWS;
import br.com.omni.autenticacao.starter.http.domain.GerarTokenRequest;
import br.com.omni.commons.domain.Autenticacao;
import br.com.omni.commons.json.JsonUtils;
import br.com.omni.idwall.starter.api.util.IdwallWSUtil;
import br.com.omni.idwall.starter.config.IdwallApiProperties;
import br.com.omni.idwall.starter.domain.FindSelfieURLResponse;
import br.com.omni.idwall.starter.http.client.IdWallReportWS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IdWallReportWSImpl implements IdWallReportWS {

	private final IdwallApiProperties properties;
	private final RestTemplate restTemplate;
    private final AutenticacaoWS autenticacaoService;
    
    public static final String SELFIE_RESOURCE =  "/api/internal/report/selfie";
	
    public IdWallReportWSImpl(IdwallApiProperties properties, RestTemplate restTemplate,
			AutenticacaoWS autenticacaoService) {
		this.properties = properties;
		this.restTemplate = restTemplate;
		this.autenticacaoService = autenticacaoService;
	}
    
	@Override
	public FindSelfieURLResponse findSelfieUrl(String token) {
		 log.info("IdWallReportWSImpl findSelfieUrl entrada {}", token);
    	 StringBuilder base = new StringBuilder(properties.getEndpoint()).append(SELFIE_RESOURCE);
    	 String url = UriComponentsBuilder.fromHttpUrl(base.toString()).pathSegment(token).build().toUriString();
         
         try {
             HttpEntity<?> entity = new HttpEntity<>(buildHeaders(obterAutenticacao()));
             var saida =  restTemplate.exchange(url, HttpMethod.GET, entity, FindSelfieURLResponse.class).getBody();
             log.info("IdWallReportWSImpl retorno {}", JsonUtils.toJson(saida));
             return saida;
         } catch (HttpClientErrorException e) {
        	 log.error("IdWallReportWSImpl error {}", e);
             throw IdwallWSUtil.excecaoDeNegocio(e);
         }
	}
	
	

	private Autenticacao obterAutenticacao() {
        try {
            GerarTokenRequest gerarTokenRequest = new GerarTokenRequest();
            gerarTokenRequest.setDuracao(10);
            gerarTokenRequest.setLogin(properties.getLogin());
            return autenticacaoService.gerarToken(gerarTokenRequest).getAutenticacao();
        } catch (TokenException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpHeaders buildHeaders(Autenticacao autenticacao) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("omni-autenticacao", toJson(autenticacao));
        headers.setContentType(APPLICATION_JSON);
        headers.setAccept(asList(APPLICATION_JSON));
        return headers;
    }

	

	

}
