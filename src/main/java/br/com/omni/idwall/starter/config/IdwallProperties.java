package br.com.omni.idwall.starter.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(prefix = "app.omni.idwall")
@Data
public class IdwallProperties {
	private String authorization;
}
