package br.com.omni.idwall.starter.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class IdwallImagemResponseVO implements Serializable {

    private Long id;
    private String urlImagem;
    private Long codigoRelatorio;
    private Integer sequencia;
}
