package br.com.omni.idwall.starter.config.amqp;

public final class Queues {

    private Queues() {}

    public static final String ROUTING_KEY_REQUEST = "ha.omni.idwall.report.event";
    public static final String EXCHANGE_REQUEST = "ha.omni.idwall.report.event";
    public static final String EXCHANGE_RESPONSE = "ha.omni.idwall.report.response.event";
}
