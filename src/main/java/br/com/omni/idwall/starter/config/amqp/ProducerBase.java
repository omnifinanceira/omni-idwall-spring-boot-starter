package br.com.omni.idwall.starter.config.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@Setter
@RequiredArgsConstructor
public class ProducerBase {

    @Autowired
    private final ObjectMapper objectMapper;

    public String stringifyRabbitMessage(final Object data) {
        try {
            return objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            log.error("object couldnt be serialized get a json string", e);
        }
        return data.toString();
    }


}
