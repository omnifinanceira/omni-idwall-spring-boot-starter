package br.com.omni.idwall.starter.http.client.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import br.com.omni.commons.exception.UnauthorizedException;
import br.com.omni.idwall.starter.exception.ImageNotFoundException;

public class IdWallFileWSUtil {
	public static RuntimeException excecaoDeNegocio(HttpClientErrorException e) {
		
		if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) 
			return new ImageNotFoundException();
		
		if(e.getStatusCode().equals(HttpStatus.UNAUTHORIZED))
			return new UnauthorizedException();
		
		return e;
	}

}
