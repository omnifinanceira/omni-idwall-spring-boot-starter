package br.com.omni.idwall.starter.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class MensagemRelatorioIdwallResponse implements Serializable {
    private IdwallResponseVO idwall;
}
