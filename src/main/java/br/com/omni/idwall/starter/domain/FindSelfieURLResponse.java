package br.com.omni.idwall.starter.domain;

import java.io.Serializable;

import br.com.omni.commons.http.domain.response.DefaultResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindSelfieURLResponse extends DefaultResponse implements Serializable {
    private String url;
}
