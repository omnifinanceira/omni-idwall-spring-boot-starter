package br.com.omni.idwall.starter.domain.enums;

import lombok.Getter;

@Getter
public enum StatusDocumento {
    OK,
    ILEGIVEL
}
