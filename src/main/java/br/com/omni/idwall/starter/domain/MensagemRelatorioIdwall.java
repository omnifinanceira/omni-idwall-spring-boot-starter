package br.com.omni.idwall.starter.domain;

import br.com.omni.idwall.starter.domain.enums.TipoRelatorioIdwall;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class MensagemRelatorioIdwall implements Serializable {
    private String tokenIdwall;
    private TipoRelatorioIdwall tipoRelatorioIdwall;
    private String cpf;
    private Long nsuImgSelfie;
    private Long nsuImgDocFrente;
    private Long nsuImgDocVerso;
    private Long nsuImgDocCompleta;
}
