package br.com.omni.idwall.starter.config.amqp;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;

import static br.com.omni.rabbitmq.common.connection.starter.config.amqp.RabbitMQCommonConsumerConfig.RABBIT_ADMIN_COMMON;

@Configuration
public class RelatorioIdwallQueuesConfiguration {

    public static final String EXCHANGE = "ha.omni.idwall.report.response.event";
    public static final String ROUTING_KEY = "${spring.application.name}";
    public static final String HA_OMNI_IDWALL_REPORT_RESPONSE_QUEUE = "ha.omni.idwall.report.response.${spring.application.name}.queue";

    private static final String DLQ_KEY_PROPERTY = "x-dead-letter-routing-key";
    private static final String X_DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";
    private static final String DLQ = ".dlq";
    private static final String DLX = ".dlx";

    private final RabbitAdmin rabbitAdmin;
    private final String routingKey;
    private final String queueName;

    public RelatorioIdwallQueuesConfiguration(@Qualifier(RABBIT_ADMIN_COMMON) RabbitAdmin rabbitAdmin,
                                              @Value(ROUTING_KEY) String routingKey,
                                              @Value(HA_OMNI_IDWALL_REPORT_RESPONSE_QUEUE) String queueName) {
        this.rabbitAdmin = rabbitAdmin;
        this.routingKey = routingKey;
        this.queueName = queueName;
    }

    @PostConstruct
    public void createQueues() {
        final Queue queue = createDefaultQueue();
        final Queue dlq = createDefaultDLQ();
        final Exchange exchange = ExchangeBuilder.topicExchange(EXCHANGE).durable(true).build();
        final Exchange dlx = ExchangeBuilder.topicExchange(EXCHANGE + DLX).durable(true).build();
        final Binding rabbitBinding = BindingBuilder.bind(queue).to(exchange).with(routingKey).and(new HashMap<>());
        final Binding bindingDlx = BindingBuilder.bind(dlq).to(dlx).with(routingKey).and(new HashMap<>());

        rabbitAdmin.declareQueue(queue);
        rabbitAdmin.declareQueue(dlq);
        rabbitAdmin.declareExchange(exchange);
        rabbitAdmin.declareExchange(dlx);
        rabbitAdmin.declareBinding(rabbitBinding);
        rabbitAdmin.declareBinding(bindingDlx);
    }

    private Queue createDefaultQueue() {
        return QueueBuilder.durable(getQueue())
                .withArgument(X_DEAD_LETTER_EXCHANGE, EXCHANGE + DLX)
                .withArgument(DLQ_KEY_PROPERTY, routingKey)
                .build();
    }

    private String getQueue() {
        return queueName;
    }

    private Queue createDefaultDLQ() {
        return QueueBuilder.durable(getQueue() + DLQ)
                .withArgument(X_DEAD_LETTER_EXCHANGE, EXCHANGE + DLX)
                .withArgument(DLQ_KEY_PROPERTY, routingKey)
                .build();
    }

}
