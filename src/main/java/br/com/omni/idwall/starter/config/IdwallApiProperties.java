package br.com.omni.idwall.starter.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(prefix = "omni.idwall")
@Data
public class IdwallApiProperties {
	private String login;
	private String endpoint;
}
