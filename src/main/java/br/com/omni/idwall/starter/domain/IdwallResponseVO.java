package br.com.omni.idwall.starter.domain;

import br.com.omni.idwall.starter.domain.enums.StatusRelatorio;
import br.com.omni.idwall.starter.domain.enums.TipoRelatorioIdwall;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class IdwallResponseVO implements Serializable {

    private Long id;
    private String tokenIdWall;
    private String numeroRelatorio;
    private StatusRelatorio statusRelatorio;
    private TipoRelatorioIdwall tipoRelatorio;
    private Double porcentagemFacematch;
    private boolean identico;
    private String replyRouting;
    private List<IdwallImagemResponseVO> imagens = new ArrayList<>();
    private DadosDocumentoResponseVO dadosDocumento;
    private String cpf;
    private Long nsuImgSelfie;
    private Long nsuImgDocFrente;
    private Long nsuImgDocVerso;
    private Long nsuImgDocCompleta;
}
