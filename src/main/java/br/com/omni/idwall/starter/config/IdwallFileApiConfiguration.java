package br.com.omni.idwall.starter.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.omni.idwall.starter.http.client.IdWallFileWS;
import br.com.omni.idwall.starter.http.client.impl.IdWallFileWSImpl;

@Configuration
@EnableConfigurationProperties(IdwallProperties.class)
public class IdwallFileApiConfiguration {

	private final IdwallProperties properties;

	public IdwallFileApiConfiguration(IdwallProperties properties) {
		this.properties = properties;
	}

	@Bean
	public IdWallFileWS idWallFileWS() {
		return new IdWallFileWSImpl(properties);
	}

}
