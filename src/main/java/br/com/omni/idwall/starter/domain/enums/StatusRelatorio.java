package br.com.omni.idwall.starter.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public enum StatusRelatorio {
    @JsonProperty("PRE-PROCESSANDO")
    PREPROCESSANDO,
    PROCESSANDO,
    PENDENTE,
    CONCLUIDO
}
