package br.com.omni.idwall.starter.http.client.impl;

import static java.util.Objects.isNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.util.Base64;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;

import br.com.omni.idwall.starter.config.IdwallProperties;
import br.com.omni.idwall.starter.exception.ErroConsultaImageException;
import br.com.omni.idwall.starter.exception.ImageNotFoundException;
import br.com.omni.idwall.starter.http.client.IdWallFileWS;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class IdWallFileWSImpl implements IdWallFileWS {

	private static final int REDIRECT = HttpStatus.SEE_OTHER.value();
	private final IdwallProperties properties;

	@Override
	public Resource findImage(String link) {
		return getFile(link);
	}

	@Override
	public String findImageBase64Encoded(String link) {
		log.info("findImageBase64Encoded => recuperando imagem idwall link => {} ", link);
		return removeMetaDadosImagem(downloadImage(link));

	}

	private Resource getFile(String link) {
		return toFile(downloadImage(link));
	}

	private byte[] downloadImage(String link) {
		try {

			log.info("recuperando imagem idwall link => {} ", link);

			HttpRequest request = HttpRequest.newBuilder().uri(new URI(link))
					.header("Authorization", properties.getAuthorization()).GET().build();

			HttpResponse<String> response = HttpClient.newBuilder().build().send(request, BodyHandlers.ofString());

			if (response.statusCode() == REDIRECT) {
				var location = response.headers().map().get("location").stream().findFirst().get();
				log.info("link idwall redirecionado para {}", location);
				HttpRequest requestByteArray = HttpRequest.newBuilder().uri(new URI(location)).GET().build();

				HttpResponse<byte[]> responseByteArray = HttpClient.newBuilder().build().send(requestByteArray,
						BodyHandlers.ofByteArray());

				if (isNull(responseByteArray.body()))
					throw new ImageNotFoundException();

				log.info("imagem {} encontrada", link);
				return responseByteArray.body();
			}

			throw new ImageNotFoundException();

		}

		catch (Exception e) {
			log.error("IdWallFileWSImpl erro => ", e);
			throw new ErroConsultaImageException(e);
		}
	}

	private String removeMetaDadosImagem(byte[] data) {

		try (var is = new ByteArrayInputStream(data); 
			 var os = new ByteArrayOutputStream();) {
			var renderedImage = ImageIO.read(is);
			ImageIO.write(renderedImage, "jpeg", os);
			return Base64.getEncoder().encodeToString(os.toByteArray());

		} catch (Exception e) {
			throw new RuntimeException("Não foi possível criar o arquivo local", e);
		}
	}
	
	private Resource toFile(byte[] data) {
		try {

			var temp = Files.createTempFile(UUID.randomUUID().toString(), "_omnidoc_upload").toFile();
			var file = new File(temp.getAbsolutePath() + ".jpeg");
			createTempFile(file);
			log.info("arquivo {} criado temp dir", file.getAbsolutePath());
			try (FileOutputStream fos = new FileOutputStream(file)) {
				fos.write(data);
				fos.flush();
			}

			return new PathResource(file.getPath());
		} catch (Exception e) {
			throw new RuntimeException("Não foi possível criar o arquivo local", e);
		}
	}

	private void createTempFile(File file) throws IOException {
		if (!file.createNewFile()) {
			throw new ErroConsultaImageException("Não foi possível criar o arquivo temporario");
		}
	}

}
