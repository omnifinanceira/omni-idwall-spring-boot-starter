package br.com.omni.idwall.starter.exception;

public class NegocioException extends  RuntimeException {
    public NegocioException() {
    }

    public NegocioException(String message) {
        super(message);
    }
}
