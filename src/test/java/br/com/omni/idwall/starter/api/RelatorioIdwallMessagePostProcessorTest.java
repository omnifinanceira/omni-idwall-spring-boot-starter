package br.com.omni.idwall.starter.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.Address;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RelatorioIdwallMessagePostProcessorTest {

    public static final String HA_OMNI_RELATORIO_IDWALL_RESPONSE_EVENT = "ha.omni.idwall.report.response.event";
    public static final String REPLY_ROUTING_TEST = "reply-routing-test";

    private RelatorioIdwallMessagePostProcessor messagePostProcessor;

    @Test
    public void testar(){
        Message message = new Message("teste".getBytes(), new MessageProperties());

        messagePostProcessor = new RelatorioIdwallMessagePostProcessor(REPLY_ROUTING_TEST);

        messagePostProcessor.postProcessMessage(message);

        Address addressExpected = new Address(HA_OMNI_RELATORIO_IDWALL_RESPONSE_EVENT, REPLY_ROUTING_TEST);

        assertEquals("application/json", message.getMessageProperties().getContentType());
        assertEquals(addressExpected, message.getMessageProperties().getReplyToAddress());
    }
}
