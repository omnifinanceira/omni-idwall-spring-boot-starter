package br.com.omni.idwall.starter.api;

import br.com.omni.idwall.starter.config.RelatorioIdwallAutoConfiguration;
import br.com.omni.idwall.starter.domain.enums.TipoRelatorioIdwall;
import br.com.omni.rabbitmq.common.connection.starter.config.JacksonParserConfiguration;
import br.com.omni.rabbitmq.common.connection.starter.config.amqp.RabbitMQCommonConsumerConfig;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import static org.junit.Assert.assertNotNull;

@Ignore
public class RelatorioIdwallIT {

    private AnnotationConfigApplicationContext context;

    @Test
    public void deveDispararRelatorioIdwall() {
        RelatorioIdwall relatorioIdwall = this.context.getBean(RelatorioIdwall.class);
        assertNotNull(relatorioIdwall);

        relatorioIdwall.disparar("sdk-bace5d4a-e9a1-4a5a-bb82-d76cacf1356f",
            TipoRelatorioIdwall.FACEMATCH_OCR_CNH);
    }

    @Test
    public void deveDispararRelatorioIdwallImagens() {
        RelatorioIdwall relatorioIdwall = this.context.getBean(RelatorioIdwall.class);
        assertNotNull(relatorioIdwall);

        relatorioIdwall.dispararComImagens(
            "61781031070",
            20712696L,
            20712696L,
            20712696L,
            null);
    }

    @Before
    public void setup() {
        load(EmptyConfiguration.class,"spring.datasource.url=jdbc:oracle:thin:@10.20.1.111:1522:OMNIDEV",
            "spring.datasource.username=SUPERVISOR",
            "spring.datasource.password=omnidev",
            "spring.datasource.driver.class=oracle.jdbc.driver.OracleDriver",
            "spring.rabbitmq.addresses=192.168.0.60",
            "spring.rabbitmq.port=5672",
            "spring.rabbitmq.username=common",
            "spring.rabbitmq.password=secret",
            "spring.rabbitmq.virtualHost=common",
            "spring.application.name=idwall-starter"
        );
    }

    @Configuration
    static class EmptyConfiguration {}

    private void load(Class<?> config, String... environment) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        TestPropertyValues.of(environment).applyTo(applicationContext);
        applicationContext.register(config);
        applicationContext.register(DataSourceAutoConfiguration.class);
        applicationContext.register(JdbcTemplateAutoConfiguration.class);
        applicationContext.register(RabbitAutoConfiguration.class);
        applicationContext.register(RelatorioIdwallAutoConfiguration.class);
        applicationContext.register(RabbitMQCommonConsumerConfig.class);
        applicationContext.register(JacksonParserConfiguration.class);
        applicationContext.refresh();
        this.context = applicationContext;
    }

}
