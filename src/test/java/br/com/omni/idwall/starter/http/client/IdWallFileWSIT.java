package br.com.omni.idwall.starter.http.client;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import br.com.omni.idwall.starter.config.IdwallFileApiConfiguration;
@Ignore
public class IdWallFileWSIT {

	//link privado
	private static final String LINK_IMAGEM_IDWALL = "https://files.idwall.co/file/idwall-ocr-cnhs/b3afb7c1-8022-4ae4-aa51-6bf238488f78/doc_54497e64-bdb6-43ce-968f-2b791d30f21d5d8b40f9-a240-46d5-9c99-352ada2c3ee2_front.jpeg";
	//private static final String LINK_IMAGEM_IDWALL = "https://www.google.com";
	//link publico
	//private static final String LINK_IMAGEM_IDWALL = "https://files.idwall.co/file/idwall-facematch/7ff70bbc-8f71-438a-bb83-7ed7a4c5882c/fm_0b14850c-9a48-4800-801b-bcdea5baa7c119261907-4d7a-4ea8-b4f5-3b9ad0d262342ce02aea-10d6-4d3a-91fe-e466dbd06637_0.jpeg";
	private AnnotationConfigApplicationContext context;

	@Test
	public void deveConsultarImagemPorLink() throws IOException {
		var idWallFileWS = context.getBean(IdWallFileWS.class);
		var retorno = idWallFileWS.findImage(
				LINK_IMAGEM_IDWALL);

		assertNotNull(retorno);
	}
	
	@Test
	public void deveConsultarImagemEncodadaBase64PorLink() throws IOException {
		var idWallFileWS = context.getBean(IdWallFileWS.class);
		var retorno = idWallFileWS.findImageBase64Encoded(
				LINK_IMAGEM_IDWALL);
		
		assertNotNull(retorno);
	}

	@Before
	public void config() {
		
		
		load(EmptyConfiguration.class, "spring.profiles.active=local", "spring.cloud.config.label=local",
				"spring.cloud.config.enabled=false",
				"app.omni.idwall.authorization=62eb98dc-6c6c-4e4c-a47f-91b35d595647",
				"spring.application.name=idwall-starter", "logging.level.org.springframework.web.client=DEBUG");

	}

	private void load(Class<?> config, String... environment) {

		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		TestPropertyValues.of(environment).applyTo(applicationContext);
		applicationContext.register(config);
		applicationContext.register(IdwallFileApiConfiguration.class);
		applicationContext.refresh();
		this.context = applicationContext;
	}

	@Configuration
	static class EmptyConfiguration {
	}
}
