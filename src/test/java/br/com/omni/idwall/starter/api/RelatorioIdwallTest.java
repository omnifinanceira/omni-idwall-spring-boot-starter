package br.com.omni.idwall.starter.api;

import br.com.omni.idwall.starter.config.amqp.ProducerBase;
import br.com.omni.idwall.starter.domain.enums.TipoRelatorioIdwall;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static br.com.omni.idwall.starter.config.amqp.Queues.EXCHANGE_REQUEST;
import static br.com.omni.idwall.starter.config.amqp.Queues.ROUTING_KEY_REQUEST;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RelatorioIdwallTest {

    private RelatorioIdwall relatorioIdwall;

    @Mock
    private RabbitTemplate rabbitTemplate;

    @Mock
    private ProducerBase producerBase;

    private String replyToRoutingDefault= "omni-teste";

    @Test
    public void deveDispararRelatorioIdwall() {
        relatorioIdwall = new RelatorioIdwall(rabbitTemplate, producerBase, replyToRoutingDefault);

        String eventJson = buildEventJson();

        doReturn(eventJson).when(producerBase).stringifyRabbitMessage(any());

        ArgumentCaptor<RelatorioIdwallMessagePostProcessor> arg = ArgumentCaptor.forClass(RelatorioIdwallMessagePostProcessor.class);

        doAnswer(invocation -> null)
            .when(rabbitTemplate)
            .convertAndSend(eq(EXCHANGE_REQUEST),
                eq(ROUTING_KEY_REQUEST),
                eq(eventJson),
                arg.capture());

        relatorioIdwall.disparar("sdk-bace5d4a-e9a1-4a5a-bb82-d76cacf1356f", TipoRelatorioIdwall.FACEMATCH_OCR_CNH);

        assertEquals("omni-teste", arg.getValue().getReplyRouting());

        verify(rabbitTemplate).convertAndSend(eq(EXCHANGE_REQUEST),
            eq(ROUTING_KEY_REQUEST),
            eq(eventJson),
            any(MessagePostProcessor.class));
    }

    @Test
    public void deveDispararRelatorioIdwallImagens() {
        relatorioIdwall = new RelatorioIdwall(rabbitTemplate, producerBase, replyToRoutingDefault);

        String eventJson = buildEventJson();

        doReturn(eventJson).when(producerBase).stringifyRabbitMessage(any());

        ArgumentCaptor<RelatorioIdwallMessagePostProcessor> arg = ArgumentCaptor.forClass(RelatorioIdwallMessagePostProcessor.class);

        doAnswer(invocation -> null)
            .when(rabbitTemplate)
            .convertAndSend(eq(EXCHANGE_REQUEST),
                eq(ROUTING_KEY_REQUEST),
                eq(eventJson),
                arg.capture());

        relatorioIdwall.dispararComImagens(
            "61781031070",
            20712696L,
            20712696L,
            20712696L,
            null);

        assertEquals("omni-teste", arg.getValue().getReplyRouting());

        verify(rabbitTemplate).convertAndSend(eq(EXCHANGE_REQUEST),
            eq(ROUTING_KEY_REQUEST),
            eq(eventJson),
            any(MessagePostProcessor.class));
    }

    private String buildEventJson() {
        return "{\"creationDate\":1558368553290,\"content\":[{\"tokenIdwall\":\"sdk-bace5d4a-e9a1-4a5a-bb82-d76cacf1356f\", \"tipoRelatorioIdwall\":\"FACEMATCH_OCR_CNH\"}],\"trackingId\":\"IDWALL-REPORT-sdk-bace5d4a-e9a1-4a5a-bb82-d76cacf1356f\"}";
    }


}
