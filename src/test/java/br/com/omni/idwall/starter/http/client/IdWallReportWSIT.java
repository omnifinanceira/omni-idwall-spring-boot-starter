package br.com.omni.idwall.starter.http.client;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import br.com.omni.idwall.starter.config.IdwallFileApiConfiguration;
@Ignore
public class IdWallReportWSIT {

	private AnnotationConfigApplicationContext context;

	@Test
	public void deveConsultarImagemPorLink() throws IOException {
		var idWallFileWS = context.getBean(IdWallReportWS.class);
		
		var retorno = idWallFileWS.findSelfieUrl("sdk-c2d08770-2c5f-4f9d-9317-525fbe5aee75");

		assertNotNull(retorno.getUrl());
		
		
	}

	@Before
	public void config() {
		
		
		load(EmptyConfiguration.class, "spring.profiles.active=local", "spring.cloud.config.label=local",
				"spring.cloud.config.enabled=false",
				"omni.idwall.login=OMNIBANCOAPP",
				"omni.idwall.endpoint=http://dev-omni-idwall.omniaws.local",
				"spring.application.name=idwall-starter", "logging.level.org.springframework.web.client=DEBUG");

	}

	private void load(Class<?> config, String... environment) {

		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		TestPropertyValues.of(environment).applyTo(applicationContext);
		applicationContext.register(config);
		applicationContext.register(IdwallFileApiConfiguration.class);
		applicationContext.refresh();
		this.context = applicationContext;
	}

	@Configuration
	static class EmptyConfiguration {
	}
}
